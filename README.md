# Selenium WebDriver Basics for Java

The example code contained within this repository was developed as supplementary materials for my [Selenium WebDriver Basics Course.](http://courses.ultimateqa.com/courses/SeleniumWebDriverForJava) The course covers the essential information a tester needs to begin developing browser automation scripts in Java, from installing a JDK and IDE through running tests using popular browser testing services. Each of the folders contains code corresponding to a section of the course and may not be complete, since some of the items are intended as exercises to be completed by the students.

## Automation service examples in this project were made possible by:

[![SauceLabs](/images/SauceLabs.png)](https://saucelabs.com) [![BrowserStack](/images/BrowserStack.png)](https://browserstack.com)

package xyz.wolvesbaneacademy.tutorials.selenium;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

/**
 * Created by SPConlin on 6/14/2017.
 */
public class Browser {

    /**
     * Starts a local Firefox browser instance
     * @return
     */
    public static WebDriver getFirefox() {
        return new FirefoxDriver();
    }

    /**
     * Starts a local Chrome browser instance
     * @return
     */
    public static WebDriver getChrome() {
        return new ChromeDriver();
    }

    /**
     * Starts a local Edge browser instance
     * @return
     */
    public static WebDriver getEdge() {
        return new EdgeDriver();
    }

    /**
     * Starts a local IE browser instance
     * @return
     */
    public static WebDriver getIE() {
        return new InternetExplorerDriver();
    }

    /**
     * Connect to a Selenium Grid or remote WebDriver server and requests the browser specified in the settings
     * @param driverHub The url:port of the remote WebDriver or Selenium Grid
     * @param settings
     * @return
     * @throws MalformedURLException
     */
    public static WebDriver connnectToRemoteWebDriver(String driverHub, HashMap<String, String> settings) throws MalformedURLException {
        return new RemoteWebDriver(new URL(driverHub + "/wd/hub/"), setCapabilities(settings));
    }

    /**
     * Connects to SauceLabs and requests the browser specified in the settings
     * @param username Your SauceLabs user ID
     * @param APIKey Your SauceLabs API Key
     * @param settings Desired capability settings
     * @return WebDriver
     * @throws MalformedURLException
     */
    public static WebDriver connectToSauceLabs(String username, String APIKey, HashMap<String, String> settings) throws MalformedURLException {
        return new RemoteWebDriver(new URL("https://" + username + ":" + APIKey + "@ondemand.saucelabs.com:443/wd/hub/"), setCapabilities(settings));
    }

    /**
     * Connects to BrowserStack and requests the browser specified in the settings
     * @param username Your BrowserStack user ID
     * @param APIKey Your BrowserStack API Key
     * @param settings Desired capability settings
     * @return WebDriver
     * @throws MalformedURLException
     */
    public static WebDriver connectToBrowserStack(String username, String APIKey, HashMap<String, String> settings) throws MalformedURLException {
        return new RemoteWebDriver(new URL("https://" + username + ":" + APIKey + "@hub-cloud.browserstack.com/wd/hub/"), setCapabilities(settings));
    }

    /**
     * Accepts a HashMap containing the capability settings desired for the browser and returns a DesiredCapabilities
     * object.
     * @param settings HashMap<String, String>
     * @return DesiredCapabilities
     */
    private static DesiredCapabilities setCapabilities(HashMap<String, String> settings) {
        DesiredCapabilities capabilities = new DesiredCapabilities();

        for (HashMap.Entry<String, String> entry : settings.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();

            switch(key.toLowerCase()) {
                case "browsername":
                    switch (value.toLowerCase()) {
                        case "chrome":
                            capabilities = DesiredCapabilities.chrome();
                            break;
                        case "internet explorer":
                            capabilities = DesiredCapabilities.internetExplorer();
                            break;
                        case "edge":
                            capabilities = DesiredCapabilities.edge();
                            break;
                        case "firefox":
                            capabilities = DesiredCapabilities.firefox();
                            break;
                        default:
                    }
                    break;
                case "browserversion":
                    capabilities.setVersion(value);
                    break;
                case "platform":
                    capabilities.setPlatform(Platform.fromString(value));
                    break;
                default:
                    capabilities.setCapability(key, value);
            }
        }

        return capabilities;
    }
}

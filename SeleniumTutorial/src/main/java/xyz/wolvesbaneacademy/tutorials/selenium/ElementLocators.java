package xyz.wolvesbaneacademy.tutorials.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;

/**
 * Created by SPConlin on 6/7/2017.
 * This class provides examples of each of tht eight element locator types and how to use them.
 */
public class ElementLocators {

    public static void main(String[] args) throws Exception {

        // Start a new instance of Chrome
        WebDriver chrome = new ChromeDriver();
        // Navigate to a test page
        chrome.get("https://courses.ultimateqa.com/users/sign_in");
        
        // ID: Locates elements using their unique ID.
        By emailLocator = By.id("user_email");
        WebElement emailField = chrome.findElement(emailLocator);
        emailField.sendKeys("someone@somewhere.com");


        // Name: Locates elements using the name attribute.
        //       This locator can be used to find multiple elements in one search
        By emailLocatorName = By.name("user[email]");
        WebElement emailByName = chrome.findElement(emailLocatorName);
        emailByName.clear();


        // XPath: Finds elements using path expressions to interrogate the HTML DOM
        //       This locator can be used to find multiple elements in one search
        By passwordField = By.xpath("//input[@placeholder = 'Password']");
        WebElement passwordByXpath = chrome.findElement(passwordField);
        passwordByXpath.sendKeys("password");

        // CSS Selector: Finds elements using the same patterns used by CSS to identify them.
        //       This locator can be used to find multiple elements in one search
        By signinLocator = By.cssSelector("input[type='submit'][name='commit']");
        WebElement signinBtn = chrome.findElement(signinLocator);
        signinBtn.click();

        // Link Text: Finds link elements with an exact match to the specified string.
        //       This locator can be used to find multiple elements in one search
        By forgotPasswordLocator = By.linkText("Forgot Password?");
        WebElement forgotPassword = chrome.findElement(forgotPasswordLocator);
        forgotPassword.click();

        // return to previous page
        chrome.navigate().back();

        // Partial Link Text: Finds link elements that contain a specified string.
        //       This locator can be used to find multiple elements in one search
        By newAccountLocator = By.partialLinkText("new account");
        WebElement newAccountLink = chrome.findElement(newAccountLocator);
        newAccountLink.click();

        // Class name: Finds Elements using a CSS Class.
        //       This locator can be used to find multiple elements in one search
        By formControlLocator = By.className("form-control");
        List<WebElement> formElements = chrome.findElements(formControlLocator);
        for (WebElement element:formElements) {
            element.sendKeys(element.getAttribute("class"));
        }

        // Tag name: Finds elements based on their HTML tag.
        //       This locator can be used to find multiple elements in one search
        By inputLocator = By.tagName("input");
        List<WebElement> inputElements = chrome.findElements(inputLocator);
        System.out.println("Number of elements found: " + inputElements.size());

        // Wait 5 seconds before closing the browser
        Thread.sleep(5000);

        // Close the Browser
        chrome.quit();
    }

}

package xyz.wolvesbaneacademy.tutorials.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * Created by SPConlin on 5/30/2017.
 */
public class InstantiateDrivers {

    public static void main(String[] args) {

        // Open a new Firefox browser locally
        // Navigate to the Wolvesbane Academy website
        // Maximize the browser
        // Close the browser
        WebDriver firefox = new FirefoxDriver();
        firefox.get("https://www.wolvesbaneacademy.xyz");
        firefox.manage().window().maximize();
        firefox.quit();

        // Open a new Chrome browser locally
        // Navigate to the Wolvesbane Academy website
        // Maximize the browser
        // Close the browser
        WebDriver chrome = new ChromeDriver();
        chrome.get("https://www.wolvesbaneacademy.xyz");
        chrome.manage().window().maximize();
        chrome.quit();

        // Open a new MS Edge browser locally
        // Navigate to the Wolvesbane Academy website
        // Maximize the browser
        // Close the browser
        WebDriver edge = new EdgeDriver();
        edge.get("https://www.wolvesbaneacademy.xyz");
        edge.manage().window().maximize();
        edge.close();
    }
}

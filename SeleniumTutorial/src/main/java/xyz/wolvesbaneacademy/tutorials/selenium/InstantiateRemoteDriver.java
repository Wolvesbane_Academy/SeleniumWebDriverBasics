package xyz.wolvesbaneacademy.tutorials.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by SPConlin on 6/3/2017.
 */
public class InstantiateRemoteDriver {

    public static void main(String[] args) throws MalformedURLException {

        // Connect to a Selenium Grid running on your local machine. If you want to connect to a Grid on
        // a different machine, change localhost to the desired machine.
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        WebDriver remote = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub/"), capabilities);
        remote.manage().window().maximize();
        remote.get("https://wolvesbaneacademy.xyz");
        remote.quit();

        // Connect to SauceLabs
        // Requires Program Arguments to be added to the Runtime Configuration
        // SauceLabs uses the first two arguments (separated by spaces) for the UserName and AccessKey
        DesiredCapabilities cap1 = DesiredCapabilities.chrome();
        WebDriver SauceLabs = new RemoteWebDriver(new URL("https://" + args[0] + ":" + args[1] + "@ondemand.saucelabs.com:443/wd/hub/"), cap1);
        SauceLabs.manage().window().maximize();
        SauceLabs.get("https://wolvesbaneacademy.xyz");
        SauceLabs.quit();

        // Connect to BrowserStack
        // Requires Program Arguments to be added to the Runtime Configuration
        // BrowserStack uses the third and fourth arguments (separated by spaces) for the UserName and AccessKey
        DesiredCapabilities cap2 = new DesiredCapabilities();
        cap2.setCapability("browser", "Chrome");
        WebDriver BrowserStack = new RemoteWebDriver(new URL("https://" + args[2] + ":" + args[3] + "@hub-cloud.browserstack.com/wd/hub/"), cap1);
        BrowserStack.manage().window().maximize();
        BrowserStack.get("https://wolvesbaneacademy.xyz");
        BrowserStack.quit();
    }
}
